import matplotlib.pyplot as plt
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).total_seconds()/3600 )):
        yield start_date + datetime.timedelta(hours=n)


start_date = datetime.datetime(2019, 1, 1)
end_date = datetime.datetime(2019, 8, 2)

t = []
d = []

old_temp = -5
for date in daterange(start_date, end_date):
    d.append(date)
    new_temp = external_temperature(date, old_temp)
    print(date, new_temp)
    t.append(new_temp)
    old_temp = new_temp

plt.plot(d,t)
plt.show()


import matplotlib.pyplot as plt

w = []
t = []
MAX_WATTAGE = 10000
for watt in range(0,MAX_WATTAGE):
    w.append(watt)
    t.append(heating(watt))

plt.plot(w,t)
plt.show()

import matplotlib.pyplot as plt

w = []
t = []
MAX_WATTAGE = 10000
for open_perc in range(0,100,1):
    w.append(open_perc)
    t.append(ventilation(open_perc, 50, 20))

plt.plot(w,t)
plt.show()