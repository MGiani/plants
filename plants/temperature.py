import math
import datetime
import calendar
import random

def external_temperature(dt, yesterday=None):

    MIN_T = -10
    MAX_T = 30

    MIN_INCREASE = 2
    MAX_INCREASE = 6

    x = dt.timetuple().tm_yday

    max_daily_increase = MIN_INCREASE + (MAX_INCREASE - MIN_INCREASE) * math.sin (x * math.pi / (365) )
    base_temp = MIN_T + (MAX_T - MIN_T) * math.sin( x * math.pi / 365)
    hourly_variation = max_daily_increase * math.sin( (dt.hour - 8) * math.pi / 12)

    forecast = base_temp + hourly_variation

    if yesterday is not None:
        DT = (forecast-yesterday) * random.uniform(0,2) + random.randint(-2,2)
        return yesterday + DT
    else:
        return forecast + random.randint(-2,2)

def heating(wattage):

    MAX_DT = 50
    return MAX_DT*(1-math.exp(-wattage/3000))

def ventilation(open_percentage, internal_T, external_T):

    if open_percentage > 100:
        raise RuntimeError('OpenPercentage > 100.')

    VENT_COEFF=50

    if internal_T >= external_T:
        return external_T + (internal_T-external_T)*(math.exp(-open_percentage/VENT_COEFF))
    if external_T > internal_T:
        return internal_T + (external_T-internal_T)*(1-math.exp(-open_percentage/VENT_COEFF))


def compute_temperature(ext_t, wattage, open_percentage):
    return ventilation(open_percentage, ext_t + heating(wattage), ext_t)

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).total_seconds()/3600 )):
        yield start_date + datetime.timedelta(hours=n)


import matplotlib.pyplot as plt

start_date = datetime.datetime(2019, 1, 1)
end_date = datetime.datetime(2019, 1, 4)

t0 = []
t1 = []
d = []

old_temp = -5
for dt in daterange(start_date, end_date):
    ext_T = external_temperature(dt)

    int_T = compute_temperature(ext_T, 5000,0)
    print(dt, 0,0, int_T)
    t0.append(int_T)
    int_T = compute_temperature(ext_T,5000,10)
    print(dt, 0,5000, int_T)

    t1.append(int_T)
    d.append(dt)

    plt.plot(d,t0)
    plt.plot(d,t1)
plt.show()